﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public interface IContractorRepository
    {
        IQueryable<Contractor> Contractors { get; }

        Task SaveContractor(Contractor c, CancellationToken token = default);
        Task CreateContractor(Contractor c, CancellationToken token = default);
        Task DeleteContractor(Contractor c, CancellationToken token = default);
    }

    public class EFContractorRepository : IContractorRepository
    {
        private DatabaseContext context;
        public IQueryable<Contractor> Contractors => context.Contractors;

        public EFContractorRepository(DatabaseContext ctx)
        {
            context = ctx;
        }

        public async Task CreateContractor(Contractor c, CancellationToken token = default)
        {
            context.Contractors.Add(c);
            await context.SaveChangesAsync(token);
        }

        public async Task DeleteContractor(Contractor c, CancellationToken token = default)
        {
            context.Contractors.Remove(c);
            await context.SaveChangesAsync(token);
        }

        public async Task SaveContractor(Contractor c, CancellationToken token = default)
        {
            await context.SaveChangesAsync(token);
        }
    }
}
