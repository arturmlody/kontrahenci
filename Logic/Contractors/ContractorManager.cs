﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Logic.Contractors
{
    public interface IContractorManager
    {
        Task<bool> Create(string name, string nip, string description, int typeId, bool isActive, CancellationToken token = default);
        List<Contractor> ReadAll();
        Task<Contractor> ReadOne(string nip, CancellationToken token = default);
        Task<bool> Update(string nip, string name, string description, bool isActive, CancellationToken token = default);
        Task<bool> Delete(string nip, CancellationToken token = default);
    }
    public class ContractorManager : IContractorManager
    {
        private readonly IContractorRepository repo;
        public ContractorManager(IContractorRepository repo)
        {
            this.repo = repo;
        }

        public async Task<bool> Create(string name, string nip, string description, int typeId, bool isActive, CancellationToken token = default)
        {
            var contractor = new Contractor();
            contractor.Name = name;
            contractor.IsActive = isActive;
            contractor.Description = description;
            contractor.Nip = nip;
            contractor.TypeId = typeId;
            await repo.CreateContractor(contractor, token);
            return true;
        }

        public async Task<bool> Delete(string nip, CancellationToken token = default)
        {
            if (nip == null)
            {
                return false;
            }
            var contractor = repo.Contractors
                .Where(c => c.Nip == nip)
                .FirstOrDefault();
            if (contractor == null)
            {
                return false;
            }
            await repo.DeleteContractor(contractor, token);
            return true;
        }

        public List<Contractor> ReadAll()
        {
            var result = repo.Contractors
                .OrderBy(c => c.Name)
                .AsNoTracking()
                .ToList();
            return result;
        }

        public async Task<bool> Update(string nip, string name, string description, bool isActive, CancellationToken token = default)
        {
            if (nip == null)
            {
                return false;
            }
            var contractor = repo.Contractors
                .Where(c => c.Nip == nip)
                .FirstOrDefault();
            if (contractor == null)
            {
                return false;
            }
            contractor.Name = name;
            contractor.Description = description;
            contractor.IsActive = isActive;
            await repo.SaveContractor(contractor, token);
            return true;
        }

        public async Task<Contractor> ReadOne(string nip, CancellationToken token = default)
        {
            var result = await repo.Contractors
                .Where(c => c.Nip == nip)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            return result;
        }
    }
}
