﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    public interface ITypeRepository
    {
        IQueryable<ContractorType> Types { get; }
        Task CreateType(ContractorType ct, CancellationToken token = default);
    }

    public class EFTypeRepository : ITypeRepository
    {
        private DatabaseContext context;
        public IQueryable<ContractorType> Types => context.ContractorTypes;

        public EFTypeRepository(DatabaseContext ctx)
        {
            context = ctx;
        }

        public async Task CreateType(ContractorType ct, CancellationToken token = default)
        {
            context.ContractorTypes.Add(ct);
            await context.SaveChangesAsync(token);
        }
    }
}
