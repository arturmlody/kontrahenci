﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Logic.Contractors
{
    public interface ITypeManager
    {
        Task<bool> Create(string name, CancellationToken token = default);
        List<ContractorType> ReadAll(CancellationToken token = default);
    }

    public class TypeManager : ITypeManager
    {
        private readonly ITypeRepository repo;
        public TypeManager(ITypeRepository repo)
        {
            this.repo = repo;
        }

        public async Task<bool> Create(string name, CancellationToken token = default)
        {
            var type = new ContractorType();
            type.Name = name;
            await repo.CreateType(type, token);
            return true;
        }

        public List<ContractorType> ReadAll(CancellationToken token = default)
        {
            var result = repo.Types
                .OrderBy(t => t.Name)
                .AsNoTracking()
                .ToList();
            return result;
        }
    }
}
