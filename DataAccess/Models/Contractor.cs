﻿namespace DataAccess.Models
{
    public class Contractor
    {
        public string Name { get; set; }
        public string Nip { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public ContractorType Type { get; set; }
        public int TypeId { get; set; }
    }
}
