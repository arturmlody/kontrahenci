﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Reflection;

namespace DataAccess.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options) { }

        public DatabaseContext(string connectionString) 
            : base(GetOptions(connectionString)) { }

        public DbSet<Contractor> Contractors { get; set; }
        public DbSet<ContractorType> ContractorTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            var contractor = builder.Entity<Contractor>();
            contractor.Property(x => x.Name).IsRequired();
            contractor.HasKey(x => x.Nip);

            var type = builder.Entity<ContractorType>();
            type.Property(x => x.Name).IsRequired();
            type.HasKey(x => x.Id);
            type.HasMany(x => x.Contractors)
                .WithOne(x => x.Type)
                .HasForeignKey(x => x.TypeId);
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
        }
    }

    public class DatabaseContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        public DatabaseContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            var migrationsAssembly = typeof(DatabaseContextFactory).GetTypeInfo().Assembly.GetName().Name;
            optionsBuilder.UseSqlServer(@"Server=(LocalDb)\MSSQLLocalDB;Database=Kontrahenci;Trusted_Connection=True;MultipleActiveResultSets=true;", b => b.MigrationsAssembly(migrationsAssembly));

            return new DatabaseContext(optionsBuilder.Options);
        }
    }
}
