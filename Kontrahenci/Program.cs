using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Kontrahenci;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using Logic.Contractors;

var host = Host.CreateDefaultBuilder().UseContentRoot(Directory.GetCurrentDirectory())
    .ConfigureServices((hostContext, services) =>
    {
        services.AddScoped<DefaultView>();
        services.AddLogging(configure => configure.AddConsole());
        services.AddDbContext<DatabaseContext>(opts =>
            opts.UseSqlServer(hostContext.Configuration.GetConnectionString("Kontrahy")));
        services.AddTransient<IContractorRepository, EFContractorRepository>();
        services.AddTransient<ITypeRepository, EFTypeRepository>();
        services.AddTransient<IContractorManager, ContractorManager>();
        services.AddTransient<ITypeManager, TypeManager>();
        services.AddTransient<EditorView>();
        services.AddTransient<DefaultView>();

    }).Build();

using (var serviceScope = host.Services.CreateScope())
{
    var services = serviceScope.ServiceProvider;
    var view = services.GetRequiredService<DefaultView>();
    Application.Run(view);
}