﻿using System.Collections.Generic;

namespace DataAccess.Models
{
    public class ContractorType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Contractor> Contractors { get; set; } = new HashSet<Contractor>();
    }
}
