﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using BrightIdeasSoftware;
using DataAccess.Models;
using Logic.Contractors;

namespace Kontrahenci
{
    public partial class DefaultView : Form
    {
        private IServiceProvider serviceProvider;
        private IContractorManager manager;

        public DefaultView(IContractorManager manager, IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
            this.manager = manager;
            InitializeComponent();
            Refresher();
        }

        private void dodaj_Click(object sender, EventArgs e)
        {
            var editorView = (EditorView)serviceProvider.GetService(typeof(EditorView));
            editorView.Show();
        }

        public void Refresher()
        {
            objectListView1.SetObjects(manager.ReadAll());
            objectListView1.AllColumns.Add(new OLVColumn()
            {
                Text = "NIP",
                Width = 100,
                AspectName = nameof(Contractor.Nip),
            });
            objectListView1.AllColumns.Add(new OLVColumn()
            {
                Text = "Nazwa",
                Width = 350,
                AspectName = nameof(Contractor.Name),
            });

            objectListView1.RebuildColumns();
        }

        private void objectListView1_CellClick(object sender, CellClickEventArgs e)
        {
            objectListView1.FullRowSelect = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var editorView = (EditorView)serviceProvider.GetService(typeof(EditorView));
            editorView.Show();
        }
    }
}
