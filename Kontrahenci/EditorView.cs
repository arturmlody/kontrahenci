﻿using Logic.Contractors;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using DataAccess.Models;

namespace Kontrahenci
{
    public partial class EditorView : Form
    {

        private ITypeRepository repo;
        private ITypeManager typeManager;
        private IContractorManager ctrManager;
        private IServiceProvider serviceProvider;

        public EditorView(ITypeManager typeManager, IContractorManager ctrManager, ITypeRepository repo, IServiceProvider serviceProvider)
        {
            
            
            InitializeComponent();
            checkBox1.Checked = true;
            this.serviceProvider = serviceProvider;
            this.typeManager = typeManager;
            this.ctrManager = ctrManager;
            this.repo = repo;
            var typeList = this.typeManager.ReadAll();
            foreach (ContractorType type in typeList)
            {
                comboBox1.Items.Add(type.Name);
            }
        }



        private void textBox3_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text.Length != 10)
            {
                MessageBox.Show("NIP musi zawierać dokładnie 10 cyfr");
                ((TextBox)sender).Focus();
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (!Regex.IsMatch(boxNip.Text, "^[0-9]*$"))
            {
                boxNip.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string select = (string)comboBox1.SelectedItem;

            var selection = repo.Types
                .Where(t => t.Name == select)
                .FirstOrDefault();

            if (!boxNip.ReadOnly)
            {
                ctrManager.Create(boxNazwa.Text,
                    boxNip.Text,
                    boxOpis.Text,
                    selection.Id,
                    checkBox1.Checked
                    );
            }
            else
            {
                ctrManager.Update(boxNip.Text,
                    boxNazwa.Text,
                    boxOpis.Text,
                    checkBox1.Checked
                    );
            }
        }

        private void EditorView_FormClosed(object sender, FormClosedEventArgs e)
        {
            var defaultView = (DefaultView)serviceProvider.GetService(typeof(DefaultView));
            defaultView.Refresher();
        }
    }
}
